<?php
/*
	Main Program Configuration
	
	This file stores all the main information for the system to function.
	
	It also will check if a user already has a cookie for the site, and will
	log them back in automatically.
	
	THIS PAGE MUST BE INCLUDED ON EVERY PAGE
*/

session_start();

$GLOBALS['config'] = array(
	'mysql' => array(
		'host' => '127.0.0.1',
		'username' => 'root',
		'password' => 'root',
		'db' => 'example'
	),
	'remember' => array(
		'cookieName' => 'hash',
		'cookieExpiry' => 604800
	),
	'session' => array(
		'sessionName' => 'user',
		'tokenName' => 'token'
	),
	'pbkdf2' => array(
		'rounds' => 10000,
		'algo' => 'sha256',
		'keySize' => 32
	)
);

spl_autoload_register(function($class) 
{
	require_once 'classes/' . $class . '.php';
});

require_once 'functions/sanitize.php';


if (Cookie::exists(Config::get('remember/cookieName')) && !Session::exists(Config::get('session/sessionName')))
{
	// Get Hash from Cookie
	$hash = Cookie::get(Config::get('remember/cookieName'));
	
	// Check to see if Hash from Cookie is in Database
	$dbHash = DB::getInstance()->get('users_session', array('hash', '=', $hash));
	
	// If Hash is in Database, Log the user back in.
	if ($dbHash->count())
	{
		$user = new User($dbHash->first()->user_id);
		$user->login();
	}
}
