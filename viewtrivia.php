<?php
require_once 'core/init.php';

$db = DB::getInstance();

$triviaInfo = $db->get('trivia_examples', array('id', '>', 0))->results();

?>
<table border="1">
	<tr>
		<td>Question</td>
		<td>Answer A</td>
		<td>Answer B</td>
		<td>Answer C</td>
		<td>Answer D</td>
		<td>Correct Answer</td>
		<td>Submitted By</td>
	</tr>
<?php

foreach ($triviaInfo as $info)
{
	$user = new User($info->user_id);
	
	$username = $user->data()->username;

	echo '<tr>';
	echo '<td>' . $info->question . '</td>';
	echo '<td>' . $info->answer_a . '</td>';
	echo '<td>' . $info->answer_b . '</td>';
	echo '<td>' . $info->answer_c . '</td>';
	echo '<td>' . $info->answer_d . '</td>';
	echo '<td>' . strtoupper(substr($info->correct_answer, -1)) . '</td>';
	echo '<td>' . $username . '</td>';
}
	
?>
</table>

