<?php
require_once 'core/init.php';
 
if (Input::exists())
{
	if (Token::check(Input::get('token')))
	{
		$validate = new Validate();
		
		$validation = $validate->check($_POST, array(
			'username' => array(
				'id' => 'Username',
				'required' => true,
				'min' => 2,
				'max' => 20,
				'unique' => 'users',
				'isNumeric' => false,
				'alphanumeric' => true
			),
			'password' => array(
				'id' => 'Password',
				'required' => true,
				'min' => 6
			),
			'password_again' => array(
				'id' => 'Repeat Password',
				'id2' => 'Password',
				'required' => true,
				'matches' => 'password'
			)
		));
		
		if ($validation->passed())
		{
			$user = new User();
			
			try
			{
				$user->create(array(
					'username' => Input::get('username'),
					'password' => Crypto::PBKDF2(Input::get('password'), Crypto::generateSalt(), Config::get('pbkdf2/rounds')),
					'joined'   => date('Y-m-d H:i:s'),
					'group'    => 1
				));
				
				Session::flash('home', 'You have been registered and can now login.');
				Redirect::to('index.php');
			}
			catch (Exception $e)
			{
				die($e->getMessage());
			}
		}
		else
		{
			foreach ($validation->errors() as $error)
			{
				echo $error, '<br>';
			}
		}
	}
}
?>

<form action="" method ="post">
	<div class="field">
		<label for="username">Username: </label>
		<input type="text" name="username" id="username" value="<?php echo escape(Input::get('username')); ?>" autocomplete="off">
	</div>
	<div class="field">
		<label for="password">Password: </label>
		<input type="password" name="password" id="password">
	</div>
	<div class="field">
		<label for="password_again">Repeat Password: </label>
		<input type="password" name="password_again" id="password_again">
	</div>
	<input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
	<input type="submit" value="Register">
</form>
