<?php
require_once 'core/init.php';

$user = new User();

if (!$user->isLoggedIn())
{
	Redirect::to('index.php');
}

if (Input::exists())
{
	if (Token::check(Input::get('token')))
	{
		$validate = new Validate();
		
		$validation = $validate->check($_POST, array(
			'trivia_question' => array(
				'id' => 'Trivia Question',
				'required' => true,
				'min' => 6
			),
			'answer_a' => array(
				'id' => 'Answer A',
				'required' => true,
				'min' => 6
			),
			'answer_b' => array(
				'id' => 'Answer B',
				'required' => true,
				'min' => 6
			),
			'answer_c' => array(
				'id' => 'Answer C',
				'required' => true,
				'min' => 6
			),
			'answer_d' => array(
				'id' => 'Answer D',
				'required' => true,
				'min' => 6
			),
			'answers' => array(
				'id' => 'Correct Answer',
				'required' => true
			)
		));
		
		if ($validation->passed())
		{
			try
			{
				$db = DB::getInstance();
			
				$db->insert('trivia_examples', array(
					'user_id'  => $user->data()->id,
					'question' => escape(Input::get('trivia_question')),
					'answer_a' => escape(Input::get('answer_a')),
					'answer_b' => escape(Input::get('answer_b')),
					'answer_c' => escape(Input::get('answer_c')),
					'answer_d' => escape(Input::get('answer_d')),
					'correct_answer' => escape(Input::get('answers'))
				));
				
				Session::flash('home', 'You have submitted a trivia question!');
				Redirect::to('index.php');
			}
			catch (Exception $e)
			{
				die($e->getMessage());
			}
		}
		else
		{
			foreach ($validation->errors() as $error)
			{
				echo $error, '<br>';
			}
		}
	}
}
?>
<form action="" method ="post">
	<div class="field">
		<label for="trivia_question">Question: </label>
		<input type="text" name="trivia_question" id="trivia_question" value="<?php echo escape(Input::get('trivia_question')); ?>" autocomplete="off">
	</div>
	<div class="field">
		<label for="answer_a">Answer A: </label>
		<input type="text" name="answer_a" id="answer_a" value="<?php echo escape(Input::get('answer_a')); ?>" autocomplete="off">
	</div>
	<div class="field">
		<label for="answer_b">Answer B: </label>
		<input type="text" name="answer_b" id="answer_b" value="<?php echo escape(Input::get('answer_b')); ?>" autocomplete="off">
	</div>
	<div class="field">
		<label for="answer_c">Answer C: </label>
		<input type="text" name="answer_c" id="answer_c" value="<?php echo escape(Input::get('answer_c')); ?>" autocomplete="off">
	</div>
	<div class="field">
		<label for="answer_d">Answer D: </label>
		<input type="text" name="answer_d" id="answer_d" value="<?php echo escape(Input::get('answer_d')); ?>" autocomplete="off">
	</div>
	<div class="field">
		<fieldset>
			<legend>Correct Answer:</legend>
			<input id="answer_a" type="radio" name="answers" value="answer_a" checked="checked">
			<label for="answer_a">A</label><br>
			<input id="answer_b" type="radio" name="answers" value="answer_b">
			<label for="answer_b">B</label><br>
			<input id="answer_c" type="radio" name="answers" value="answer_c">
			<label for="answer_c">C</label><br>
			<input id="answer_d" type="radio" name="answers" value="answer_d">
			<label for="answer_d">D</label><br>
		</fieldset>
	</div>
	<input type="hidden"  name="token" value="<?php echo Token::generate(); ?>">
	<input type="submit" value="Update">
</form>
