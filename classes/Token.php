<?php
/**
	Token Class
	
	This class is exclusively for prevent CSRF (Cross Site Request Forgery)
*/

class Token 
{

	public static function generate() 
	{
		return Session::put(Config::get('session/tokenName'), Crypto::randomHash());
	}

	public static function check ($token) 
	{
		$tokenName = Config::get('session/tokenName');
		
		if (Session::exists($tokenName) && Crypto::secureCompare($token, Session::get($tokenName)))
		{
			Session::delete($tokenName);
			return true;
		}
		
		return false;
	}
}
