<?php
/**
	User Class
	
	Provides functionality to help dealing with users.
	
	Using the Database Class Example, you can do that task easier in the User
	class.
	----------------------------------------------------------------------------
	
	$user = new User(5);
	
	// Get Username from User Data
	$username = $user->data()->username;
*/

class User 
{

	private $_db, $_data, $_settings, $_recovery, $_session, 
			$_sessionName, $_cookieName, $_isLoggedIn;
	
	public function __construct ($user = null)
	{
		$this->_db = DB::getInstance();
		$this->_sessionName = Config::get('session/sessionName');
		$this->_cookieName  = Config::get('remember/cookieName');
		
		if (!$user)
		{
			if (Session::exists($this->_sessionName))
			{
				$user = Session::get($this->_sessionName);
				
				if ($this->find($user))
				{
					$this->_isLoggedIn = true;
				}
				else
				{
					// Logout
				}
			}
		}
		else
		{
			$this->find($user);
		}
	}
	
	public function update ($area, $fields = array(), $id = null)
	{
		if ($id === null) 
		{
			$id = $this->data()->id;
		}
		
		switch ($area)
		{
			case 'data':
				if (!$this->_db->update('users', 'id', $id, $fields))
				{
					throw new Exception('There was a problem updating your account.');
				}
				break;
			
			// Other Cases Could Be Add

		}
	}
	
	public function create ($fields = array())
	{
		if (!$this->_db->insert('users', $fields))
		{
			throw new Exception('There was a problem creating your account.');
		}
	}
	
	public function find ($user = null)
	{
		if ($user)
		{
			$field = (is_numeric($user)) ? 'id' : 'username'; // Usernames must have at least one letter.
			
			$data = $this->_db->get('users', array($field, '=', $user));
			
			if ($data->count())
			{
				$this->_data = $data->first();
				
				return true;
			}
		}
		
		return false;
	}
	
	public function login($username = null, $password = null, $remember = false)
	{
		if(!$username && !$password && $this->exists())
		{
			Session::put($this->_sessionName, $this->data()->id);
		}
		else
		{
			$user = $this->find($username);
			
			if ($user)
			{
				if (Crypto::passwordCompare($this->data()->password, $password))
				{
					Session::put($this->_sessionName, $this->data()->id);
					
					if ($remember)
					{
						$rememberHash = Crypto::randomHash();
						
						$checkHash = $this->_db->get('users_sessions', array('user_id', '=', $this->data()->id));
						
						if (!$checkHash->count())
						{
							$this->_db->insert('users_sessions', array('user_id' => $this->data()->id, 'hash' => $rememberHash));
						}
						else
						{
							$rememberHash = $checkHash->first()->hash;
						}
						
						Cookie::put($this->_cookieName, $rememberHash, Config::get('remember/cookieExpiry'));
					}
					
					// Compare Iteration Count between Settings and Database
					
					// If they are different iteration counts, rehash the password with a new hash
					// with a updated iteration count and enter it into the database.
					
					if (Config::get('pbkdf2/rounds') !== Crypto::splitPassword($this->data()->password, 'rounds'))
					{
							$this->update('data', array(
								'password' => Crypto::PBKDF2($password, Crypto::generateSalt(), Config::get('pbkdf2/rounds'))
							));
					}
					
					return true;
				}
			}
		}
		
		return false;
	}
	
	public function hasPermission ($key)
	{
		$group = $this->_db->get('groups', array('id', '=', $this->data()->group));
		
		if ($group->count())
		{
			$permissions = json_decode($group->first()->permissions, true);
			
			if ($permissions[$key])
			{
				return true;
			}
		}
		
		return false;
	}

	public function exists() 
	{
		return (!empty($this->_data)) ? true : false;
	}

	public function logout() 
	{
		$this->_db->delete('users_sessions', array('user_id', '=', $this->data()->id));
		
		Session::delete($this->_sessionName);
		Cookie::delete($this->_cookieName);
	}

	public function data() 
	{
		return $this->_data;
	}

	public function isLoggedIn() 
	{
		return $this->_isLoggedIn;
	}	
}
