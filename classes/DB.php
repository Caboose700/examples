<?php
/**
	Database Class

	This class is all about Databases. This is the central location for 
	accessing the MySQL database.
	
	This class makes it much simpler to use Databases, and also has the added
	benefit of being secure as well.
	
	Say you wanted to get the username of a user, knowing the user's id. You 
	would craft your result like this.
	----------------------------------------------------------------------------
	
	$db = DB::getInstance; // Get Instance of the Database Class
	
	// SELECT * from users where 'user_id' = 5;
	$searchQuery = $db->get('users', array('user_id', '=', 5)); 
	
	// Get username from the First Result
	$username = $searchQuery->first()->username;
*/

class DB 
{
	
	private static $_instance = null;
	private $_pdo, $_query, $_error = false, $_results, $_count = 0;
	
	private function __construct()
	{
		try {
			$this->_pdo = new PDO('mysql:host=' . Config::get('mysql/host') . ';dbname=' . Config::get('mysql/db'), Config::get('mysql/username'), Config::get('mysql/password'));
			
			$this->_pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); // Do Not Emulate Prepared Statements
		}
		catch (PDOException $e)
		{
			die($e->getMessage());
		}
	}
	
	public static function getInstance()
	{
		if(!isset(self::$_instance))
		{
			self::$_instance = new DB();
		}
		return self::$_instance;
	}
	
	public function query ($sql, $params = array())
	{
		$this->_error = false;
		
		if ($this->_query = $this->_pdo->prepare($sql))
		{
			$count = 1;
			
			if (count($params))
			{
				foreach ($params as $param)
				{
					$this->_query->bindValue($count, $param);
					$count++;
				}
			}
			
			if ($this->_query->execute())
			{
				$this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
				$this->_count = $this->_query->rowCount();
			} 
			else
			{
				$this->_error = true;
			}
		}
		return $this;
	}
	
	public function action ($action, $table, $where = array())
	{
		if (count($where) === 3)
		{
			$operators = array('=', '>', '<', '>=', '<=');
			
			$field    = $where[0];
			$operator = $where[1];
			$value    = $where[2];
			
			if (in_array($operator, $operators))
			{
				$sql = "{$action} from {$table} WHERE {$field} {$operator} ?;";
				
				if (!$this->query($sql, array($value))->error())
				{
					return $this;
				}
			}
		}
		
		return false;
	}
	
	public function get ($table, $where)
	{
		return $this->action('SELECT *', $table, $where);
	}
	
	public function delete ($table, $where)
	{
		return $this->action('DELETE', $table, $where);
	}
	
	public function insert ($table, $fields = array())
	{
		$keys = array_keys($fields);
		
		$values = '';
		$count = 1;
		
		foreach ($fields as $field)
		{
			$values .= '?';
			
			if ($count < count($fields))
			{
				$values .= ', ';
			}
			
			$count++;
		}
		
		$sql = "INSERT INTO {$table} (`" . implode('`, `', $keys) . "`) VALUES ({$values});";
		
		if (!$this->query($sql, $fields)->error())
		{
			return true;
		}
		
		return false;
	}
	
	public function update ($table, $where, $id, $fields)
	{
		$set = '';
		$count = 1;
		
		foreach ($fields as $name => $value)
		{
			$set .= "{$name} = ?";
			
			if ($count < count($fields))
			{
				$set .= ', ';
			}
			
			$count++;
		}
		
		$sql = "UPDATE {$table} SET {$set} WHERE {$where} = '{$id}';";
		
		if (!$this->query($sql, $fields)->error())
		{
			return true;
		}
		
		return false;
	}

	public function results() 
	{
		return $this->_results;
	}
	
	public function first() 
	{
		return $this->results()[0];
	}
	
	public function error() 
	{
		return $this->_error;
	}
	
	public function count() 
	{
		return $this->_count;
	}
}
