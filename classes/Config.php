<?php
/**
	Config Class
	
	Allows the programmer to pull config information out of the init.php array
	in a friendly enviorment.
	
	For example, a programmer can use "Config::get('pbkdf2/rounds');" to get the
	number of rounds for our PBKDF2 system.
*/

class Config 
{
	public static function get ($path = null)
	{
		if ($path)
		{
			$config = $GLOBALS['config'];
			$path = explode('/', $path);
			
			foreach ($path as $segment)
			{
				if (null !== $config[$segment])
				{
					$config = $config[$segment];
				}
			}
			
			return $config;
		}
		return false;
	}
}
