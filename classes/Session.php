<?php
/**
	Session Class
	
	Makes Using Sessions a bit easier. (Pretty Similar to the Cookie Class)
	
	One neat part of this function, is the flash function, which allows you to
	post a one time message on a page after a user does something, for example,
	submits some data.
*/

class Session 
{

	public static function exists ($name) 
	{
		return (isset($_SESSION[$name])) ? true : false;
	}

	public static function put ($name, $value) 
	{
		return $_SESSION[$name] = $value;
	}

	public static function get ($name) 
	{
		return $_SESSION[$name];
	}

	public static function delete ($name) 
	{
		if (self::exists($name)) 
		{
			unset($_SESSION[$name]);
		}
	}

	public static function flash ($name, $string = '') 
	{
		if (self::exists($name)) 
		{
			$session = self::get($name);
			self::delete($name);
			return $session;
		} 
		else 
		{
			self::put($name, $string);
		}
		
		return '';
	}
}
