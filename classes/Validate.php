<?php
/**
	
	Validation Class
	
	Provides really neat functionality for validating forms.
*/

class Validate 
{
	private $_passed = false, $_errors = array(), $_db = null;
	
	public function __construct()
	{
		$this->_db = DB::getInstance();
	}
	
	public function check ($source, $items = array())
	{
		foreach ($items as $item => $rules)
		{
			$id = '';
			
			foreach ($rules as $rule => $ruleValue)
			{
				$value = trim($source[$item]);
				$item = escape($item);
				
				if ($rule === 'id')
				{
					$id = $ruleValue;
				}
				
				if ($rule === 'id2')
				{
					$id2 = $ruleValue;
				}
				
				if ($rule === 'required' && empty($value))
				{
					$this->addError("{$id} is required!");
				}
				else if (!empty($value))
				{
					switch ($rule)
					{
						case 'min':
							if (strlen($value) < $ruleValue)
							{
								$this->addError("{$id} must be a minimum of {$ruleValue} characters!");
							}
							break;
						
						case 'max':
							if (strlen($value) > $ruleValue)
							{
								$this->addError("{$id} must be a maximum of {$ruleValue} characters!");
							}
							break;
						
						case 'matches':
							if (!Crypto::secureCompare($source[$ruleValue], $value))
							{
								$this->addError("{$id} must match {$id2}!");
							}
							break;
						
						case 'unique':
							$checkDB = $this->_db->get($ruleValue, array($item, '=', $value));
							
							if ($checkDB->count())
							{
								$this->addError("{$id} already exists!");
							}
							break;
						
						case 'isNumeric':
							if ($ruleValue === true)
							{
								if (!is_numeric($value))
								{
									$this->addError("{$id} can only be a number!");
								}
							}
							else
							{
								if (is_numeric($value))
								{
									$this->addError("{$id} must contain at least one letter!");
								}
							}
							break;
						
						case 'valueMin':
							if ($value < $ruleValue)
							{
								$this->addError("{$id} must be at least {$ruleValue}!");
							}
							break;
						
						case 'valueMax':
							if ($value > $ruleValue)
							{
								$this->addError("{$id} cannot exceed {$ruleValue}!");
							}
							break;
						
						case 'isInArray':
							$valueCheck = strtolower($value);
							
							if (!in_array($valueCheck, $ruleValue))
							{
								$this->addError(escape($value) . " is not a valid option for {$id}!");
							}
							break;
						
						case 'isNotInArray':
							$valueCheck = strtolower($value);
							
							if (in_array($valueCheck, $ruleValue))
							{
								$this->addError(escape($value) . " is not a valid option for {$id}!");
							}
							break;
							
						case 'alphanumeric':
							if (!ctype_alnum($value))
							{
								$this->addError("{$id} can only consist of Alphanumeric Characters!");
							}
							break;
					}
				}
			}
		}
		
		if (empty($this->_errors))
		{
			$this->_passed = true;
		}
		
		return $this;
	}
		
	public static function whitelist ($source, $values)
	{
		$errors = 0;
		
		foreach ($source as $key=>$item)
		{
			if (!in_array($key, $values))
			{
				$errors++;
			}
		}
		
		if ($errors === 0)
		{
			return true;
		}
		return false;
	}

	private function addError ($error) 
	{
		$this->_errors[] = $error;
	}

	public function errors() 
	{
		return $this->_errors;
	}

	public function passed() 
	{
		return $this->_passed;
	}
}
