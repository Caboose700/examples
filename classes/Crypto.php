<?php
/**
	Crypto Class
	
	This class handles all the security features of the system.
	
	This class is also the one that, if a new version of PHP is used, would get
	the most rewrites, as I wanted to make sure that this class could be used on
	a older version of PHP.
	
	This class handles Password Hashing (courtesy of PBKDF2), Timing Attack
	Protected String Comparison, and Random Hash Generation.
*/

class Crypto
{
	// Inspired by https://defuse.ca/php-pbkdf2.htm
	// If PHP is upgraded, a native solution can be used.
	public static function PBKDF2 ($password, $salt, $rounds, $format = true)
	{
		$algo = Config::get('pbkdf2/algo');
		$keySize = Config::get('pbkdf2/keySize');
	
		$hashLength = strlen(hash($algo, "", true));
		$blockCount = ceil($keySize / $hashLength);
		
		$output = "";
		
		for ($i = 1; $i <= $blockCount; $i++)
		{
			$last = $salt . pack("N", $i);
			
			// First Iteration
			$last = $xorsum = hash_hmac($algo, $last, $password, true);
			
			// Perform Rounds - 1 Iterations
			for ($j = 1; $j < $rounds; $j++)
			{
				$xorsum ^= ($last = hash_hmac($algo, $last, $password, true));
			}
			
			$output .= $xorsum;
		}
		
		$finalHash = bin2hex(substr($output, 0, $keySize));
		
		if ($format)
		{
			return $rounds . "|" . $salt . "|" . $finalHash;
		}
		else
		{
			return $finalHash;
		}
	}
	
	// Seperate Password Container into Seperate Stages
	public static function splitPassword($encodedPassword, $desired)
	{
		$array = explode("|", $encodedPassword);
		
		switch($desired)
		{
			case 'rounds':
				return $array[0];
			case 'salt':
				return $array[1];
			case 'hash':
				return $array[2];
			default:
				return null;
		}
	}
	
	// Compare Data Securely Securely (Timing Attack Prevention)
	
	// This Function is purely a workaround if system needs to be run on PHP 5.2.
	// If PHP can be upgraded, a native solution can be used.
	
	// Inspired by: http://php.net/manual/de/function.hash-equals.php#115635
	public static function secureCompare($knownData, $userProvidedData)
	{
		if (strlen($knownData) != strlen($userProvidedData))
		{
			return false;
		}
		else
		{
			$res = $knownData ^ $userProvidedData;
			$ret = 0;
			
			$r = strlen($res) - 1;
			
			for ($i = $r; $i >= 0; $i--)
			{
				$ret |= ord($res[$i]);
			}
			
			return !$ret;
		}
	}
	
	public static function passwordCompare($knownPassword, $userProvidedPassword)
	{
		$kpH = self::splitPassword($knownPassword, 'hash');
		$kpS = self::splitPassword($knownPassword, 'salt');
		$kpR = self::splitPassword($knownPassword, 'rounds');
	
		$upH = self::PBKDF2($userProvidedPassword, $kpS, $kpR, false); 
		
		return self::secureCompare($kpH, $upH);
	}
	
	// Native Linux Solution
	// If PHP is upgraded, a native solution can be used.
	public static function getRandom($bytes)
	{
		$generatedBytes = '';
		
		$urandomFile = @fopen('/dev/urandom', 'rb');
		
		if ($urandomFile !== FALSE)
		{
			$generatedBytes .= @fread($urandomFile, $bytes);
			@fclose($urandomFile);
		}
		
		return $generatedBytes;
	}
	
	public static function generateSalt()
	{
		return base64_encode(self::getRandom(16));
	}
	
	public static function randomHash()
	{
		return hash('sha256', self::getRandom(16));
	}
}
