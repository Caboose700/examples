# Example Code #

This is my (Caboose700) "resume" of sorts for MacDGuy to see my skills at PHP.

### What is this? ###

This is a "framework" of sorts that I've worked on on and off for three years. I was inspired by a whole slew of sources.

### What's it do? ###

* Easy / Secure Access to the MySQL Database
* A Crypto Class for handling Password Hashing and Comparisons.
* A whole bunch of helper functions.
* A really dead simple and basic trivia example.

### What's it need? ###

Ideally, this would be used on a newer version of PHP (5.6 or 7), but I've cobbled together some alternatives functionality that works just the same on 5.2.

The Security Functionality assumes you are on Linux.

### How do I get this setup? ###

* Drop the Code into your Web Server
* Configure the MySQL Server with the database.sql file provided
* Configure init.php in the core folder for the database.

### Who do I talk to? ###

* Caboose700 (That's Me :p)