<?php
require_once 'core/init.php';

$user = new User();

if (!$user->isLoggedIn())
{
	Redirect::to('index.php');
}

if (Input::exists())
{
	if (Token::check(Input::get('token')))
	{
		$validate = new Validate();
		
		$validation = $validate->check($_POST, array(
			'password_current' => array(
				'required' => true,
				'min' => 6
			),
			'password_new' => array(
				'required' => true,
				'min' => 6
			),
			'password_new_again' => array(
				'required' => true,
				'min' => 6,
				'matches' => 'password_new'
			)
		));
		
		if ($validation->passed())
		{
			if (Crypto::passwordCompare($user->data()->password, Input::get('password_current')))
			{
				$user->update('data', array(
					'password' => Crypto::PBKDF2(Input::get('password_new'), Crypto::generateSalt(), Config::get('pbkdf2/rounds'))
				));
				
				Session::flash('home', 'You have changed your password!');
				Redirect::to('index.php');
			}
			else
			{
				echo 'Current Password Incorrect.';
			}
		}
		else
		{
			foreach ($validation->errors() as $error)
			{
				echo $error, '<br>';
			}
		}
	}
}
?>
<form action="" method ="post">
	<div class="field">
		<label for="password_current">Current Password: </label>
		<input type="password" name="password_current" id="password_current">
	</div>
	<div class="field">
		<label for="password_new">New Password: </label>
		<input type="password" name="password_new" id="password_new">
	</div>
	<div class="field">
		<label for="password_new_again">Repeat New Password: </label>
		<input type="password" name="password_new_again" id="password_new_again">
	</div>
	<input type="hidden"  name="token" value="<?php echo Token::generate(); ?>">
	<input type="submit" value="Update">
</form>
